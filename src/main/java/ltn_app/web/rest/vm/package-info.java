/**
 * View Models used by Spring MVC REST controllers.
 */
package ltn_app.web.rest.vm;
